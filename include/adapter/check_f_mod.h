#include "qcmatrix_config.h"

#if !defined(LANG_F_MODULE)
#error "a module from Fortran 90 library required"
#endif
#if !defined(LANG_F_MATRIX)
#error "a matrix type from Fortran 90 library required"
#endif
