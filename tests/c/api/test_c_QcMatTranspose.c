/* QcMatrix: an abstract matrix library
   Copyright 2012-2015 Bin Gao

   QcMatrix is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   QcMatrix is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with QcMatrix. If not, see <http://www.gnu.org/licenses/>.

   This file tests the function QcMatTranspose().

   2014-03-28, Bin Gao:
   * first version
*/

/* header file of QcMatrix library */
#include "qcmatrix.h"
/* parameters for test suite */
#include "tests/qcmatrix_test_param.h"

/*% \brief tests the function QcMatTranspose()
    \author Bin Gao
    \date 2014-03-28
    \param[QcMat:type]{in} A the matrix
*/
QErrorCode test_c_QcMatTranspose(QcMat *A)
{
    QcMat B;                           /* duplication of the matrix A */
    QBool assembled;                   /* indicates if the matrix is assembled or not */
    QInt num_blocks;                   /* number of blocks */
    QInt dim_mat;                      /* dimension of each block */
    QInt size_values;                  /* number of elements in the matrix */
    QReal *values_real;                /* values of the real part */
    QReal *values_imag;                /* values of the imaginary part */
    QReal value_tmp;                   /* temporary value */
    QcMatOperation all_mat_operations[3] = {MAT_TRANSPOSE,
                                            MAT_HERM_TRANSPOSE,
                                            MAT_COMPLEX_CONJUGATE};
    const QReal CF_THRESHOLD=1000.0*QZEROTHRSH;
    QBool is_equal;                    /* indicates if the matrix and array have the same values */
    QInt iop, irow, icol, ival, jval;  /* incremental recorders */
    QErrorCode ierr;                   /* error information */
#if defined(QCMATRIX_ENABLE_VIEW)
    FILE *fp_mat;
#endif
    /* checks if the matrix is assembled */
    ierr = QcMatIsAssembled(A, &assembled);
    QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatIsAssembled(A)");
    if (assembled!=QTRUE) {
        printf("test_c_QcMatTranspose>> matrix A is not assembled ...\n");
        printf("test_c_QcMatTranspose>> QcMatTranspose() will not be tested ...\n");
        return QSUCCESS;
    }
    /* duplicates the matrix A, and uses the duplication for the test */
    ierr = QcMatCreate(&B);
    QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatCreate(B)");
    ierr = QcMatTranspose(MAT_NO_OPERATION, A, &B);
    QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatTranspose(MAT_NO_OPERATION, A)");
    /* checks if B = A */
    ierr = QcMatIsEqual(A, &B, QTRUE, CF_THRESHOLD, &is_equal);
    QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatIsEqual(A, B)");
    if (is_equal==QTRUE) {
        printf("test_c_QcMatTranspose>> QcMatTranspose(MAT_NO_OPERATION, A) passed ...\n");
    }
    else {
        /* dumps results to check */
#if defined(QCMATRIX_ENABLE_VIEW)
        fp_mat = fopen("test_c_QcMatTranspose.yml", "w");
        if (fp_mat==NULL) {
            QErrorExit(FILE_AND_LINE, "failed to open test_c_QcMatTranspose.yml");
        }
        ierr = QcMatWrite(A, fp_mat, ASCII_VIEW);
        QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatWrite(A)");
        ierr = QcMatWrite(&B, fp_mat, ASCII_VIEW);
        QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatWrite(B)");
        fclose(fp_mat);
#endif
        printf("test_c_QcMatTranspose>> QcMatTranspose(MAT_NO_OPERATION, A) failed\n");
        return QFAILURE;
    }
    /* gets the number of blocks */
    ierr = QcMatGetNumBlocks(A, &num_blocks);
    QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatGetNumBlocks(A)");
    /* gets the dimension of each block */
    ierr = QcMatGetDimMat(A, &dim_mat, &dim_mat);
    QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatGetDimMat(A)");
    size_values = num_blocks*num_blocks*dim_mat*dim_mat;
    /* allocates memory for the elements of the matrix */
    values_real = (QReal *)malloc(sizeof(QReal)*size_values);
    if (values_real==NULL) {
        printf("test_c_QcMatTranspose>> failed to allocate values_real\n");
        return QFAILURE;
    }
    values_imag = (QReal *)malloc(sizeof(QReal)*size_values);
    if (values_imag==NULL) {
        printf("test_c_QcMatTranspose>> failed to allocate values_imag\n");
        return QFAILURE;
    }
    for (iop=0; iop<3; iop++) {
        /* tests QcMatTranspose() out-of-place with option all_mat_operations[iop] */
        ierr = QcMatTranspose(all_mat_operations[iop], A, &B);
        QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatTranspose(A) out-of-place");
        /* gets all the values of the matrix */
        ierr = QcMatGetAllValues(A,
                                 QTRUE,
                                 size_values,
                                 values_real,
                                 values_imag);
        QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatGetAllValues(A)");
        /* manually transposes */
        switch (all_mat_operations[iop]) {
        case MAT_TRANSPOSE:
            ival = -num_blocks*dim_mat;
            for (irow=0; irow<num_blocks*dim_mat; irow++) {
                ival += num_blocks*dim_mat;
                for (icol=0; icol<irow; icol++) {
                    jval = icol*num_blocks*dim_mat+irow;
                    /* real part */
                    value_tmp = values_real[ival+icol];
                    values_real[ival+icol] = values_real[jval];
                    values_real[jval] = value_tmp;
                    /* imaginary part */
                    value_tmp = values_imag[ival+icol];
                    values_imag[ival+icol] = values_imag[jval];
                    values_imag[jval] = value_tmp;
                }
            }
            break;
        case MAT_HERM_TRANSPOSE:
            ival = -num_blocks*dim_mat;
            for (irow=0; irow<num_blocks*dim_mat; irow++) {
                ival += num_blocks*dim_mat;
                for (icol=0; icol<=irow; icol++) {
                    jval = icol*num_blocks*dim_mat+irow;
                    /* real part */
                    value_tmp = values_real[ival+icol];
                    values_real[ival+icol] = values_real[jval];
                    values_real[jval] = value_tmp;
                    /* imaginary part */
                    value_tmp = values_imag[ival+icol];
                    values_imag[ival+icol] = -values_imag[jval];
                    values_imag[jval] = -value_tmp;
                }
            }
            break;
        case MAT_COMPLEX_CONJUGATE:
            for (ival=0; ival<size_values; ival++) {
                values_imag[ival] = -values_imag[ival];
            }
            break;
        default:
            printf("test_c_QcMatTranspose>> invalid operation on matrix: %d\n",
                   all_mat_operations[iop]);
            return QFAILURE;
        }
        ierr = QcMatCfArray(&B,
                            QTRUE,
                            size_values,
                            values_real,
                            values_imag,
                            CF_THRESHOLD,
                            &is_equal);
        QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatCfArray(B)");
        if (is_equal==QTRUE) {
            printf("test_c_QcMatTranspose>> QcMatTranspose(A, %d) out-of-place passed ...\n",
                   all_mat_operations[iop]);
        }
        else {
            /* dumps results to check */
#if defined(QCMATRIX_ENABLE_VIEW)
            fp_mat = fopen("test_c_QcMatTranspose.yml", "w");
            if (fp_mat==NULL) {
                QErrorExit(FILE_AND_LINE, "failed to open test_c_QcMatTranspose.yml");
            }
            ierr = QcMatWrite(A, fp_mat, ASCII_VIEW);
            QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatWrite(A)");
            ierr = QcMatWrite(&B, fp_mat, ASCII_VIEW);
            QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatWrite(B)");
            fclose(fp_mat);
#endif
            printf("test_c_QcMatTranspose>> real part of B from hand coding\n");
            for (ival=0; ival<size_values; ival++) {
                if (ival%4==3) {
                    printf("%20.12"QREAL_FMT"\n", values_real[ival]);
                }
                else {
                    printf("%20.12"QREAL_FMT"  ", values_real[ival]);
                }
            }
            printf("test_c_QcMatTranspose>> imaginary part of B from hand coding\n");
            for (ival=0; ival<size_values; ival++) {
                if (ival%4==3) {
                    printf("%20.12"QREAL_FMT"\n", values_imag[ival]);
                }
                else {
                    printf("%20.12"QREAL_FMT"  ", values_imag[ival]);
                }
            }
            printf("test_c_QcMatTranspose>> QcMatTranspose(A) out-of-place failed\n");
            return QFAILURE;
        }
        /* tests QcMatTranspose() in-place with option all_mat_operations[iop] */
        ierr = QcMatTranspose(all_mat_operations[iop], &B, &B);
        QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatTranspose(B) in-place");
        /* manually transposes */
        switch (all_mat_operations[iop]) {
        case MAT_TRANSPOSE:
            ival = -num_blocks*dim_mat;
            for (irow=0; irow<num_blocks*dim_mat; irow++) {
                ival += num_blocks*dim_mat;
                for (icol=0; icol<irow; icol++) {
                    jval = icol*num_blocks*dim_mat+irow;
                    /* real part */
                    value_tmp = values_real[ival+icol];
                    values_real[ival+icol] = values_real[jval];
                    values_real[jval] = value_tmp;
                    /* imaginary part */
                    value_tmp = values_imag[ival+icol];
                    values_imag[ival+icol] = values_imag[jval];
                    values_imag[jval] = value_tmp;
                }
            }
            break;
        case MAT_HERM_TRANSPOSE:
            ival = -num_blocks*dim_mat;
            for (irow=0; irow<num_blocks*dim_mat; irow++) {
                ival += num_blocks*dim_mat;
                for (icol=0; icol<=irow; icol++) {
                    jval = icol*num_blocks*dim_mat+irow;
                    /* real part */
                    value_tmp = values_real[ival+icol];
                    values_real[ival+icol] = values_real[jval];
                    values_real[jval] = value_tmp;
                    /* imaginary part */
                    value_tmp = values_imag[ival+icol];
                    values_imag[ival+icol] = -values_imag[jval];
                    values_imag[jval] = -value_tmp;
                }
            }
            break;
        case MAT_COMPLEX_CONJUGATE:
            for (ival=0; ival<size_values; ival++) {
                values_imag[ival] = -values_imag[ival];
            }
            break;
        default:
            printf("test_c_QcMatTranspose>> invalid operation on matrix: %d\n",
                   all_mat_operations[iop]);
            return QFAILURE;
        }
        ierr = QcMatCfArray(&B,
                            QTRUE,
                            size_values,
                            values_real,
                            values_imag,
                            CF_THRESHOLD,
                            &is_equal);
        QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatCfArray(B)");
        if (is_equal==QTRUE) {
            printf("test_c_QcMatTranspose>> QcMatTranspose(B, %d) in-place passed ...\n",
                   all_mat_operations[iop]);
        }
        else {
            /* dumps results to check */
#if defined(QCMATRIX_ENABLE_VIEW)
            fp_mat = fopen("test_c_QcMatTranspose.yml", "w");
            if (fp_mat==NULL) {
                QErrorExit(FILE_AND_LINE, "failed to open test_c_QcMatTranspose.yml");
            }
            ierr = QcMatWrite(&B, fp_mat, ASCII_VIEW);
            QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatWrite(B)");
            fclose(fp_mat);
#endif
            printf("test_c_QcMatTranspose>> real part of B from hand coding\n");
            for (ival=0; ival<size_values; ival++) {
                printf("%e\n",values_real[ival]);
            }
            printf("test_c_QcMatTranspose>> imaginary part of B from hand coding\n");
            for (ival=0; ival<size_values; ival++) {
                printf("%e\n",values_imag[ival]);
            }
            printf("test_c_QcMatTranspose>> QcMatTranspose(B) in-place failed\n");
            return QFAILURE;
        }
    }
    /* cleans */
    free(values_real);
    values_real = NULL;
    free(values_imag);
    values_imag = NULL;
    ierr = QcMatDestroy(&B);
    QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatDestroy(B)");
    return QSUCCESS;
}
