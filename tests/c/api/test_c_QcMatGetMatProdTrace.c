/* QcMatrix: an abstract matrix library
   Copyright 2012-2015 Bin Gao

   QcMatrix is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   QcMatrix is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with QcMatrix. If not, see <http://www.gnu.org/licenses/>.

   This file tests the function QcMatGetMatProdTrace().

   2014-06-19, Bin Gao:
   * first version
*/

/* header file of QcMatrix library */
#include "qcmatrix.h"
/* some basic algebraic functions */
#include "utilities/qcmatrix_algebra.h"
/* parameters for test suite */
#include "tests/qcmatrix_test_param.h"

/*% \brief tests the function QcMatGetMatProdTrace()
    \author Bin Gao
    \date 2014-06-19
    \param[QcMat:type]{in} A the matrix
*/
QErrorCode test_c_QcMatGetMatProdTrace(QcMat *A, QcMat *B)
{
    QBool assembled;                                          /* indicates if the matrix is assembled or not */
    QInt num_blocks;                                          /* number of blocks */
    QcMat C;                                                  /* product matrix */
    QReal *cf_trace;                                          /* trace to compare with */
    QReal *trace;                                             /* trace from QcMatGetMatProdTrace() */
    const QReal CF_THRESHOLD=1000.0*QZEROTHRSH;
    QcMatOperation all_mat_operations[4] = {MAT_NO_OPERATION, /* all matrix operations */
                                            MAT_TRANSPOSE,
                                            MAT_HERM_TRANSPOSE,
                                            MAT_COMPLEX_CONJUGATE};
    QReal positive_one[2] = {1.0,0.0};                        /* positive one */
    QReal real_zero[2] = {0.0,0.0};                           /* zero */
    QInt iop, iblk, pos_real, pos_imag;                       /* incremental recorders */
    QErrorCode ierr;                                          /* error information */
#if defined(QCMATRIX_ENABLE_VIEW)
    FILE *fp_mat;
#endif
    /* checks if the matrices A and B are assembled */
    ierr = QcMatIsAssembled(A, &assembled);
    QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatIsAssembled(A)");
    if (assembled!=QTRUE) {
        printf("test_c_QcMatGetMatProdTrace>> matrix A is not assembled ...\n");
        printf("test_c_QcMatGetMatProdTrace>> QcMatGetMatProdTrace() will not be tested ...\n");
        return QSUCCESS;
    }
    ierr = QcMatIsAssembled(B, &assembled);
    QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatIsAssembled(A)");
    if (assembled!=QTRUE) {
        printf("test_c_QcMatGetMatProdTrace>> matrix A is not assembled ...\n");
        printf("test_c_QcMatGetMatProdTrace>> QcMatGetMatProdTrace() will not be tested ...\n");
        return QSUCCESS;
    }
    ierr = QcMatCreate(&C);
    QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatCreate(C)");
    /* gets the number of blocks */
    ierr = QcMatGetNumBlocks(A, &num_blocks);
    QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatGetNumBlocks(A)");
    /* allocates memory for the traces */
    cf_trace = (QReal *)malloc(sizeof(QReal)*2*num_blocks);
    if (cf_trace==NULL) {
        printf("test_c_QcMatGetMatProdTrace>> failed to allocate cf_trace\n");
        return QFAILURE;
    }
    trace = (QReal *)malloc(sizeof(QReal)*2*num_blocks);
    if (trace==NULL) {
        printf("test_c_QcMatGetMatProdTrace>> failed to allocate trace\n");
        return QFAILURE;
    }
    for (iop=0; iop<4; iop++) {
        printf("test_c_QcMatGetMatProdTrace>> operation on B %d\n",
               all_mat_operations[iop]);
        ierr = QcMatGetMatProdTrace(A, B, all_mat_operations[iop], num_blocks, trace);
        QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatGetMatProdTrace(A,B)");
        /* gets the trace by calling QcMatGEMM() and QcMatGetTrace() */
        ierr = QcMatGEMM(MAT_NO_OPERATION,
                         all_mat_operations[iop],
                         positive_one,
                         A,
                         B,
                         real_zero,
                         &C);
        QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatGEMM(A,B)");
        ierr = QcMatGetTrace(&C, num_blocks, cf_trace);
        QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatGetTrace(C)");
        for (iblk=0,pos_real=0,pos_imag=1; iblk<num_blocks; iblk++) {
            if (QAbs(trace[pos_real]-cf_trace[pos_real])>CF_THRESHOLD ||
                QAbs(trace[pos_imag]-cf_trace[pos_imag])>CF_THRESHOLD) {
                printf("test_c_QcMatGetMatProdTrace>> %"QINT_FMT": (%"QREAL_FMT",%"QREAL_FMT"), (%"QREAL_FMT",%"QREAL_FMT")\n",
                       iblk,
                       trace[pos_real],
                       trace[pos_imag],
                       cf_trace[pos_real],
                       cf_trace[pos_imag]);
#if defined(QCMATRIX_ENABLE_VIEW)
                fp_mat = fopen("test_c_QcMatGetMatProdTrace.yml", "w");
                if (fp_mat==NULL) {
                    QErrorExit(FILE_AND_LINE, "failed to open test_c_QcMatGetMatProdTrace.yml");
                }
                ierr = QcMatWrite(A, fp_mat, ASCII_VIEW);
                QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatWrite(A)");
                ierr = QcMatWrite(B, fp_mat, ASCII_VIEW);
                QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatWrite(B)");
                ierr = QcMatWrite(&C, fp_mat, ASCII_VIEW);
                QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatWrite(C)");
                fclose(fp_mat);
#endif
                printf("test_c_QcMatGetMatProdTrace>> QcMatGetMatProdTrace(A,B) failed\n");
                return QFAILURE;
            }
            pos_real += 2;
            pos_imag += 2;
        }
        printf("test_c_QcMatGetMatProdTrace>> QcMatGetMatProdTrace(A,B) passed ...\n");
    }
    ierr = QcMatDestroy(&C);
    QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatDestroy(C)");
    free(cf_trace);
    cf_trace = NULL;
    free(trace);
    trace = NULL;
    return QSUCCESS;
}
