/* QcMatrix: an abstract matrix library
   Copyright 2012-2015 Bin Gao

   QcMatrix is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   QcMatrix is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with QcMatrix. If not, see <http://www.gnu.org/licenses/>.

   This file is the test suite of C APIs.

   2014-03-13, Bin Gao:
   * first version
*/

/* header file of QcMatrix library */
#include "qcmatrix.h"
/* parameters for test suite */
#include "tests/qcmatrix_test_param.h"

/* declaration of different test routines */
extern QErrorCode test_c_QcMatValues(const QInt num_blocks);
extern QErrorCode test_c_QcMatDuplicate(QcMat *A);
extern QErrorCode test_c_QcMatZeroEntries(QcMat *A);
extern QErrorCode test_c_QcMatGetTrace(QcMat *A);
extern QErrorCode test_c_QcMatScale(QcMat *A);
extern QErrorCode test_c_QcMatAXPY(QcMat *X, QcMat *Y);
extern QErrorCode test_c_QcMatTranspose(QcMat *A);
extern QErrorCode test_c_QcMatView(QcMat *A);
extern QErrorCode test_c_QcMatGEMM(QcMat *A, QcMat *B);
extern QErrorCode test_c_QcMatGetMatProdTrace(QcMat *A, QcMat *B);

#if defined(QCMATRIX_TEST_EXECUTABLE)
QErrorCode main()
#else
QErrorCode test_c_QcMatrix()
#endif
{
    QcMat A, B;        /* matrices for tests */
    QInt num_blocks;   /* number of blocks */
    QInt dim_mat = 6;  /* dimension of each block */
    QcSymType sym_type[3] = {QSYMMAT,QANTISYMMAT,QNONSYMMAT};  /* all symmetry types */
    QcDataType data_type[3] = {QREALMAT,QIMAGMAT,QCMPLXMAT};   /* all data types */
    QChar mat_name[] = " _B S D ";
    QInt isym, jsym;   /* incremental recorders for symmetry types */
    QInt idat, jdat;   /* incremental recorders for data types */
    QInt ierr;         /* error information */
    /* tests different numbers of blocks */
    for (num_blocks=1; num_blocks<=MAX_NUM_BLOCKS; num_blocks++) {
        mat_name[3] = (QChar)(num_blocks+'0');
        /* tests QcMatSetValues() and QcMatGetValues() */
        ierr = test_c_QcMatValues(num_blocks);
        QErrorCheckCode(ierr, FILE_AND_LINE, "calling test_c_QcMatValues()");
        /* tests different symmetry types (symmetric, anti-symmetric, non-symmetric) for matrix A */
        for (isym=0; isym<3; isym++) {
            mat_name[5] = (QChar)(isym+1+'0');
            /* tests different data types (real, imaginary, complex) for matrix A */
            for (idat=0; idat<3; idat++) {
                mat_name[7] = (QChar)(idat+1+'0');
                ierr = QcMatCreate(&A);
                QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatCreate(A)");
                //if (ierr==QSUCCESS) {
                //    printf("test_c_QcMatrix>> QcMatCreate(A) passed ...\n");
                //}
                //else {
                //    printf("test_c_QcMatrix>> failed to call QcMatCreate(A)\n");
                //    exit(ierr);
                //}
                /* generates a random matrix A according to its symmetry and data types */
                ierr = QcMatSetRandMat(&A,
                                       sym_type[isym],
                                       data_type[idat],
                                       num_blocks,
                                       dim_mat,
                                       dim_mat);
                QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatSetRandMat(A)");
                //if (ierr==QSUCCESS) {
                //    printf("test_c_QcMatrix>> QcMatSetRandMat(A) passed ...\n");
                //}
                //else {
                //    printf("test_c_QcMatrix>> failed to call QcMatSetRandMat(A)\n");
                //    exit(ierr);
                //}
                mat_name[0] = 'A';
                ierr = QcMatSetName(&A, mat_name);
                QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatSetName(A)");
                //if (ierr==QSUCCESS) {
                //    printf("test_c_QcMatrix>> QcMatSetName(A) passed ...\n");
                //}
                //else {
                //    printf("test_c_QcMatrix>> failed to call QcMatSetName(A)\n");
                //    exit(ierr);
                //}
                /* tests QcMatDuplicate() */
                ierr = test_c_QcMatDuplicate(&A);
                QErrorCheckCode(ierr, FILE_AND_LINE, "calling test_c_QcMatDuplicate(A)");
                /* tests QcMatZeroEntries() */
                ierr = test_c_QcMatZeroEntries(&A);
                QErrorCheckCode(ierr, FILE_AND_LINE, "calling test_c_QcMatZeroEntries(A)");
                /* tests QcMatGetTrace() */
                ierr = test_c_QcMatGetTrace(&A);
                QErrorCheckCode(ierr, FILE_AND_LINE, "calling test_c_QcMatGetTrace(A)");
                /* tests QcMatScale() */
                ierr = test_c_QcMatScale(&A);
                QErrorCheckCode(ierr, FILE_AND_LINE, "calling test_c_QcMatScale(A)");
                /* tests QcMatAXPY() */
/*FIXME: B->name*/
                ierr = QcMatCreate(&B);
                QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatCreate(B)");
                //if (ierr==QSUCCESS) {
                    ierr = test_c_QcMatAXPY(&A, &B);
                    QErrorCheckCode(ierr, FILE_AND_LINE, "calling test_c_QcMatAXPY(A, B)");
                    ierr = QcMatDestroy(&B);
                    QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatDestroy(B)");
                //    if (ierr!=QSUCCESS) {
                //        printf("test_c_QcMatrix>> failed to call QcMatDestroy(B)\n");
                //        exit(ierr);
                //    }
                //}
                //else {
                //    printf("test_c_QcMatrix>> failed to call QcMatCreate(B)\n");
                //    exit(ierr);
                //}
                /* tests QcMatTranspose() in-place and out-of-place */
                ierr = test_c_QcMatTranspose(&A);
                QErrorCheckCode(ierr, FILE_AND_LINE, "calling test_c_QcMatTranspose(A)");
#if defined(QCMATRIX_ENABLE_VIEW)
                /* tests QcMatWrite() and QcMatRead() */
                ierr = test_c_QcMatView(&A);
                QErrorCheckCode(ierr, FILE_AND_LINE, "calling test_c_QcMatView(A)");
#endif
#if defined(ADAPTER_C_LANG)
                /* tests QcMatGetExternalMat() */
                /*test_c_QcMatGetExternalMat(&A);*/
#endif
                /* tests different symmetry types (symmetric, anti-symmetric, non-symmetric) for matrix B */
                for (jsym=0; jsym<3; jsym++) {
                    mat_name[5] = (QChar)(jsym+1+'0');
                    /* tests different data types (real, imaginary, complex) for matrix B */
                    for (jdat=0; jdat<3; jdat++) {
                        mat_name[7] = (QChar)(jdat+1+'0');
                        ierr = QcMatCreate(&B);
                        QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatCreate(B)");
                        //if (ierr==QSUCCESS) {
                        //    printf("test_c_QcMatrix>> QcMatCreate(B) passed ...\n");
                        //}
                        //else {
                        //    printf("test_c_QcMatrix>> failed to call QcMatCreate(B)\n");
                        //    exit(ierr);
                        //}
                        /* generates a random matrix B according to its symmetry and data types */
                        ierr = QcMatSetRandMat(&B,
                                               sym_type[jsym],
                                               data_type[jdat],
                                               num_blocks,
                                               dim_mat,
                                               dim_mat);
                        QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatSetRandMat(B)");
                        //if (ierr==QSUCCESS) {
                        //    printf("test_c_QcMatrix>> QcMatSetRandMat(B) passed ...\n");
                        //}
                        //else {
                        //    printf("test_c_QcMatrix>> failed to call QcMatSetRandMat(B)\n");
                        //    exit(ierr);
                        //}
                        mat_name[0] = 'B';
                        ierr = QcMatSetName(&B, mat_name);
                        QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatSetName(B)");
                        //if (ierr==QSUCCESS) {
                        //    printf("test_c_QcMatrix>> QcMatSetName(B) passed ...\n");
                        //}
                        //else {
                        //    printf("test_c_QcMatrix>> failed to call QcMatSetName(B)\n");
                        //    exit(ierr);
                        //}
                        /* tests QcMatAXPY() */
                        ierr = test_c_QcMatAXPY(&A, &B);
                        QErrorCheckCode(ierr, FILE_AND_LINE, "calling test_c_QcMatAXPY(A, B)");
                        /* tests QcMatGEMM() */
                        ierr = test_c_QcMatGEMM(&A, &B);
                        QErrorCheckCode(ierr, FILE_AND_LINE, "calling test_c_QcMatGEMM(A, B)");
                        /* tests QcMatGetMatProdTrace() */
                        ierr = test_c_QcMatGetMatProdTrace(&A, &B);
                        QErrorCheckCode(ierr, FILE_AND_LINE, "calling test_c_QcMatGetMatProdTrace(A, B)");
                        /* cleans */
                        ierr = QcMatDestroy(&B);
                        QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatDestroy(B)");
                        //if (ierr==QSUCCESS) {
                        //    printf("test_c_QcMatrix>> QcMatDestroy(B) passed ...\n");
                        //}
                        //else {
                        //    printf("test_c_QcMatrix>> failed to call QcMatDestroy(B)\n");
                        //    exit(ierr);
                        //}
                    }
                }
                /* cleans */
                ierr = QcMatDestroy(&A);
                QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatDestroy(A)");
                //if (ierr==QSUCCESS) {
                //    printf("test_c_QcMatrix>> QcMatDestroy(A) passed ...\n");
                //}
                //else {
                //    printf("test_c_QcMatrix>> failed to call QcMatDestroy(A)\n");
                //    exit(ierr);
                //}
            }
        }
    }
    return QSUCCESS;
}
