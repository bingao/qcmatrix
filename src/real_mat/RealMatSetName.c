/* QcMatrix: an abstract matrix library
   Copyright 2012-2015 Bin Gao

   QcMatrix is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   QcMatrix is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with QcMatrix. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function RealMatSetName().

   2016-10-31, Bin Gao:
   * first version
*/

#include "impls/real_mat.h"

/*% \brief sets the name of a matrix
    \author Bin Gao
    \date 2016-10-31
    \param[RealMat:struct]{in} A the matrix, should be at least assembled
        by RealMatAssemble()
    \param[char:char]{in} mat_name name of the matrix, should be unique
    \return[QErrorCode:int] error information
*/
QErrorCode RealMatSetName(RealMat *A, const char *mat_name)
{
    QSizeT len_name;  /* length of the matrix name */
    if (A->name!=NULL) {
        free(A->name);
    }
    len_name = strlen(mat_name);
    A->name = (char *)malloc(len_name+1);
    if (A->name==NULL) {
        printf("RealMatSetName>> given matrix name (%"QINT_FMT"): %s\n",
               (QInt)len_name,
               mat_name);
        QErrorExit(FILE_AND_LINE, "failed to allocate memory for matrix name");
    }
    strcpy(A->name, mat_name);
    return QSUCCESS;
}
