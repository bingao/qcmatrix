# Source codes for setting/getting the external real matrix
SET(QCMATRIX_SRCS
    ${QCMATRIX_SRCS}
    ${LIB_QCMATRIX_PATH}/src/cmplx_mat/CmplxMatSetAdapterMat.c
    ${LIB_QCMATRIX_PATH}/src/cmplx_mat/CmplxMatGetAdapterMat.c
    ${LIB_QCMATRIX_PATH}/src/qcmat/QcMatSetAdapterMat.c
    ${LIB_QCMATRIX_PATH}/src/qcmat/QcMatGetAdapterMat.c)
