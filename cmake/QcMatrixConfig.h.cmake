# File name of the configure file
SET(QCMATRIX_CONFIG ${PROJECT_BINARY_DIR}/qcmatrix_config.h)
#SET(QCMATRIX_CONFIG ${LIB_QCMATRIX_PATH}/include/qcmatrix_config.h)
# Writes all the options specified by the user
FILE(WRITE ${QCMATRIX_CONFIG} "#if !defined(QCMATRIX_CONFIG_H)\n")
FILE(APPEND ${QCMATRIX_CONFIG} "#define QCMATRIX_CONFIG_H\n\n")
IF(QCMATRIX_64BIT_INTEGER)
    FILE(APPEND ${QCMATRIX_CONFIG} "#if !defined(QCMATRIX_64BIT_INTEGER)\n")
    FILE(APPEND ${QCMATRIX_CONFIG} "#define QCMATRIX_64BIT_INTEGER\n")
    FILE(APPEND ${QCMATRIX_CONFIG} "#endif\n\n")
ENDIF()
IF(QCMATRIX_BLAS_64BIT)
    FILE(APPEND ${QCMATRIX_CONFIG} "#if !defined(QCMATRIX_BLAS_64BIT)\n")
    FILE(APPEND ${QCMATRIX_CONFIG} "#define QCMATRIX_BLAS_64BIT\n")
    FILE(APPEND ${QCMATRIX_CONFIG} "#endif\n\n")
ENDIF()
IF(QCMATRIX_ZERO_BASED)
    FILE(APPEND ${QCMATRIX_CONFIG} "#if !defined(QCMATRIX_ZERO_BASED)\n")
    FILE(APPEND ${QCMATRIX_CONFIG} "#define QCMATRIX_ZERO_BASED\n")
    FILE(APPEND ${QCMATRIX_CONFIG} "#endif\n\n")
ENDIF()
IF(QCMATRIX_ROW_MAJOR)
    FILE(APPEND ${QCMATRIX_CONFIG} "#if !defined(QCMATRIX_ROW_MAJOR)\n")
    FILE(APPEND ${QCMATRIX_CONFIG} "#define QCMATRIX_ROW_MAJOR\n")
    FILE(APPEND ${QCMATRIX_CONFIG} "#endif\n\n")
ENDIF()
IF(QCMATRIX_SINGLE_PRECISION)
    FILE(APPEND ${QCMATRIX_CONFIG} "#if !defined(QCMATRIX_SINGLE_PRECISION)\n")
    FILE(APPEND ${QCMATRIX_CONFIG} "#define QCMATRIX_SINGLE_PRECISION\n")
    FILE(APPEND ${QCMATRIX_CONFIG} "#endif\n\n")
ENDIF()
IF(QCMATRIX_STORAGE_MODE)
    FILE(APPEND ${QCMATRIX_CONFIG} "#if !defined(QCMATRIX_STORAGE_MODE)\n")
    FILE(APPEND ${QCMATRIX_CONFIG} "#define QCMATRIX_STORAGE_MODE\n")
    FILE(APPEND ${QCMATRIX_CONFIG} "#endif\n\n")
ENDIF()
IF(QCMATRIX_ENABLE_VIEW)
    FILE(APPEND ${QCMATRIX_CONFIG} "#if !defined(QCMATRIX_ENABLE_VIEW)\n")
    FILE(APPEND ${QCMATRIX_CONFIG} "#define QCMATRIX_ENABLE_VIEW\n")
    FILE(APPEND ${QCMATRIX_CONFIG} "#endif\n\n")
ENDIF()
IF(QCMATRIX_ENABLE_HDF5)
    FILE(APPEND ${QCMATRIX_CONFIG} "#if !defined(QCMATRIX_ENABLE_HDF5)\n")
    FILE(APPEND ${QCMATRIX_CONFIG} "#define QCMATRIX_ENABLE_HDF5\n")
    FILE(APPEND ${QCMATRIX_CONFIG} "#endif\n\n")
ENDIF()
IF(QCMATRIX_ENABLE_MXML)
    FILE(APPEND ${QCMATRIX_CONFIG} "#if !defined(QCMATRIX_ENABLE_MXML)\n")
    FILE(APPEND ${QCMATRIX_CONFIG} "#define QCMATRIX_ENABLE_MXML\n")
    FILE(APPEND ${QCMATRIX_CONFIG} "#endif\n\n")
ENDIF()
IF(NOT QCMATRIX_ENABLE_HDF5 OR NOT QCMATRIX_ENABLE_MXML)
    FILE(APPEND ${QCMATRIX_CONFIG} "#if !defined(QCMATRIX_STANDARD_IO)\n")
    FILE(APPEND ${QCMATRIX_CONFIG} "#define QCMATRIX_STANDARD_IO\n")
    FILE(APPEND ${QCMATRIX_CONFIG} "#endif\n\n")
ENDIF()
IF(QCMATRIX_3M_METHOD)
    FILE(APPEND ${QCMATRIX_CONFIG} "#if !defined(QCMATRIX_3M_METHOD)\n")
    FILE(APPEND ${QCMATRIX_CONFIG} "#define QCMATRIX_3M_METHOD\n")
    FILE(APPEND ${QCMATRIX_CONFIG} "#endif\n\n")
ENDIF()
IF(QCMATRIX_STRASSEN_METHOD)
    FILE(APPEND ${QCMATRIX_CONFIG} "#if !defined(QCMATRIX_STRASSEN_METHOD)\n")
    FILE(APPEND ${QCMATRIX_CONFIG} "#define QCMATRIX_STRASSEN_METHOD\n")
    FILE(APPEND ${QCMATRIX_CONFIG} "#endif\n\n")
ENDIF()
IF(QCMATRIX_AUTO_ERROR_EXIT)
    FILE(APPEND ${QCMATRIX_CONFIG} "#if !defined(QCMATRIX_AUTO_ERROR_EXIT)\n")
    FILE(APPEND ${QCMATRIX_CONFIG} "#define QCMATRIX_AUTO_ERROR_EXIT\n")
    FILE(APPEND ${QCMATRIX_CONFIG} "#endif\n\n")
ENDIF()
IF(QCMATRIX_BUILD_ADAPTER)
    FILE(APPEND ${QCMATRIX_CONFIG} "#if !defined(${ADAPTER_MATRIX_TYPE})\n")
    FILE(APPEND ${QCMATRIX_CONFIG} "#define ${ADAPTER_MATRIX_TYPE}\n")
    FILE(APPEND ${QCMATRIX_CONFIG} "#endif\n")
    # C
    IF(ADAPTER_C_LANG)
        FILE(APPEND ${QCMATRIX_CONFIG} "#if !defined(ADAPTER_C_LANG)\n")
        FILE(APPEND ${QCMATRIX_CONFIG} "#define ADAPTER_C_LANG\n")
        FILE(APPEND ${QCMATRIX_CONFIG} "#endif\n")
        FILE(APPEND ${QCMATRIX_CONFIG} "#if !defined(LANG_C_HEADER)\n")
        FILE(APPEND ${QCMATRIX_CONFIG} "#define LANG_C_HEADER ${LANG_C_HEADER}\n")
        FILE(APPEND ${QCMATRIX_CONFIG} "#endif\n")
        FILE(APPEND ${QCMATRIX_CONFIG} "#if !defined(LANG_C_MATRIX)\n")
        FILE(APPEND ${QCMATRIX_CONFIG} "#define LANG_C_MATRIX ${LANG_C_MATRIX}\n")
        FILE(APPEND ${QCMATRIX_CONFIG} "#endif\n\n")
    # C++
    ELSEIF(ADAPTER_CXX_LANG)
        FILE(APPEND ${QCMATRIX_CONFIG} "#if !defined(ADAPTER_CXX_LANG)\n")
        FILE(APPEND ${QCMATRIX_CONFIG} "#define ADAPTER_CXX_LANG\n")
        FILE(APPEND ${QCMATRIX_CONFIG} "#endif\n\n")
    # Fortran 90
    ELSEIF(ADAPTER_F90_LANG)
        FILE(APPEND ${QCMATRIX_CONFIG} "#if !defined(ADAPTER_F90_LANG)\n")
        FILE(APPEND ${QCMATRIX_CONFIG} "#define ADAPTER_F90_LANG\n")
        FILE(APPEND ${QCMATRIX_CONFIG} "#endif\n")
        FILE(APPEND ${QCMATRIX_CONFIG} "#if !defined(LANG_F_MODULE)\n")
        FILE(APPEND ${QCMATRIX_CONFIG} "#define LANG_F_MODULE ${LANG_F_MODULE}\n")
        FILE(APPEND ${QCMATRIX_CONFIG} "#endif\n")
        FILE(APPEND ${QCMATRIX_CONFIG} "#if !defined(LANG_F_MATRIX)\n")
        FILE(APPEND ${QCMATRIX_CONFIG} "#define LANG_F_MATRIX ${LANG_F_MATRIX}\n")
        FILE(APPEND ${QCMATRIX_CONFIG} "#endif\n")
        FILE(APPEND ${QCMATRIX_CONFIG} "#if !defined(SIZEOF_F_TYPE_P)\n")
        FILE(APPEND ${QCMATRIX_CONFIG} "#define SIZEOF_F_TYPE_P ${SIZEOF_F_TYPE_P}\n")
        FILE(APPEND ${QCMATRIX_CONFIG} "#endif\n\n")
    # Fortran 2003
    ELSEIF(ADAPTER_F03_LANG)
        FILE(APPEND ${QCMATRIX_CONFIG} "#if !defined(ADAPTER_F03_LANG)\n")
        FILE(APPEND ${QCMATRIX_CONFIG} "#define ADAPTER_F03_LANG\n")
        FILE(APPEND ${QCMATRIX_CONFIG} "#endif\n")
        FILE(APPEND ${QCMATRIX_CONFIG} "#if !defined(LANG_F_MODULE)\n")
        FILE(APPEND ${QCMATRIX_CONFIG} "#define LANG_F_MODULE ${LANG_F_MODULE}\n")
        FILE(APPEND ${QCMATRIX_CONFIG} "#endif\n")
        FILE(APPEND ${QCMATRIX_CONFIG} "#if !defined(LANG_F_MATRIX)\n")
        FILE(APPEND ${QCMATRIX_CONFIG} "#define LANG_F_MATRIX ${LANG_F_MATRIX}\n")
        FILE(APPEND ${QCMATRIX_CONFIG} "#endif\n\n")
    ENDIF()
ENDIF()
IF(QCMATRIX_Fortran_API)
    IF(${QCMATRIX_Fortran_API} STREQUAL "F90")
        FILE(APPEND ${QCMATRIX_CONFIG} "#if !defined(SIZEOF_VOID_P)\n")
        FILE(APPEND ${QCMATRIX_CONFIG} "#define SIZEOF_VOID_P ${CMAKE_SIZEOF_VOID_P}\n")
        FILE(APPEND ${QCMATRIX_CONFIG} "#endif\n\n")
    ENDIF()
ENDIF()
FILE(APPEND ${QCMATRIX_CONFIG} "#endif\n")

#if !defined(QCMATRIX_CONFIG_H)
#define QCMATRIX_CONFIG_H

#cmakedefine QCMATRIX_AUTO_ERROR_EXIT
#cmakedefine QCMATRIX_ENABLE_VIEW
#cmakedefine QCMATRIX_SINGLE_PRECISION
#cmakedefine QCMATRIX_STORAGE_MODE
#cmakedefine QCMATRIX_ROW_MAJOR
#cmakedefine QCMATRIX_ZERO_BASED
#cmakedefine QCMATRIX_CXX_API
#cmakedefine QCMATRIX_Fortran_API

#endif
